package com.supmti.gb.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.supmti.gb.entity.SavingsTransaction;

public interface SavingsTransactionDao extends CrudRepository<SavingsTransaction, Long> {

    List<SavingsTransaction> findAll();
}

