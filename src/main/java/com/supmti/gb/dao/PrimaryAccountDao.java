package com.supmti.gb.dao;

import org.springframework.data.repository.CrudRepository;

import com.supmti.gb.entity.PrimaryAccount;

public interface PrimaryAccountDao extends CrudRepository<PrimaryAccount,Long> {

}
