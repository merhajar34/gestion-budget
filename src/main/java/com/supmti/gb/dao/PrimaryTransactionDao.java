package com.supmti.gb.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.supmti.gb.entity.PrimaryTransaction;

public interface PrimaryTransactionDao extends CrudRepository<PrimaryTransaction, Long> {

	List<PrimaryTransaction> findAll();

}