package com.supmti.gb.dao;

import org.springframework.data.repository.CrudRepository;

import com.supmti.gb.entity.security.Role;

public interface RoleDao extends CrudRepository<Role, Integer> {

	Role findByName(String name);
}
