package com.supmti.gb.dao;

import org.springframework.data.repository.CrudRepository;

import com.supmti.gb.entity.SavingsAccount;


public interface SavingsAccountDao extends CrudRepository<SavingsAccount, Long> {
}
