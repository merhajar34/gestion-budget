package com.supmti.gb.service;

import java.security.Principal;

import com.supmti.gb.entity.PrimaryAccount;
import com.supmti.gb.entity.SavingsAccount;

public interface AccountService {

	PrimaryAccount createPrimaryAccount();
	SavingsAccount createSavingsAccount();
	
	void deposit(String accountType,String motif, double amount, Principal principal);
	void withdraw(String accountType,String motif, double amount, Principal principal);
}
